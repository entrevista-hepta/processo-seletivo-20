
Para a execução do projeto é necessário seguir alguns passos: 
- Importar o projeto como maven project no IDE de trabalho;
- Instalar o Tomcat 9 na máquina para atuar como servidor; 
- Instalar um banco de dados para guardar as informações geradas na aplicação. No projeto vem com mysql configurado e uma unidade de dados denominada 'funcionarios-bd';
- Popular o banco de dados com as informações do Setor e do Funcionario a fim de rodar a aplicação e os teste de integração;
- Há outras informações nas classes dos testes de integração que precisam ser observadas para a execução dos testes;


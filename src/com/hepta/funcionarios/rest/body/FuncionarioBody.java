package com.hepta.funcionarios.rest.body;

import com.hepta.funcionarios.entity.Funcionario;
import com.hepta.funcionarios.entity.Setor;

public class FuncionarioBody {

	private String nome;
	private Double salario;
	private String email;
	private Integer idade;
	public Integer idSetor;

	public Funcionario criarFuncionario(Setor setor) {
		Funcionario funcionario = new Funcionario();
		funcionario.setNome(nome);
		funcionario.setSalario(salario);
		funcionario.setEmail(email);
		funcionario.setIdade(idade);
		funcionario.setSetor(setor);
		return funcionario;

	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Double getSalario() {
		return salario;
	}

	public void setSalario(Double salario) {
		this.salario = salario;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getIdade() {
		return idade;
	}

	public void setIdade(Integer idade) {
		this.idade = idade;
	}

	public Integer getIdSetor() {
		return idSetor;
	}

	public void setIdSetor(Integer idSetor) {
		this.idSetor = idSetor;
	}

}

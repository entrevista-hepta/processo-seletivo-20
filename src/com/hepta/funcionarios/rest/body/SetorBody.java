package com.hepta.funcionarios.rest.body;

import com.hepta.funcionarios.entity.Setor;

public class SetorBody {
	
	private String nome;
	
	public Setor criarSetor() {
		Setor setor = new Setor();
		setor.setNome(nome);
		return setor;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	

}

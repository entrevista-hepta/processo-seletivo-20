package com.hepta.funcionarios.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.hepta.funcionarios.entity.Setor;
import com.hepta.funcionarios.persistence.SetorDAO;
import com.hepta.funcionarios.rest.body.SetorBody;

@Path("/setor")
public class SetorService {

	@Context
	private HttpServletRequest request;

	@Context
	private HttpServletResponse response;

	private SetorDAO daoSetor;
	
	public SetorService() {
		daoSetor = new SetorDAO();
	}

	public SetorService(SetorDAO daoSetor) {
		this.daoSetor = daoSetor;
	}

	/**
	 * Adiciona novo Setor
	 * 
	 * @param Setor: Novo Setor
	 * @return response 201 (CREATED) - Conseguiu adicionar
	 * @throws Exception
	 */
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@POST
	public Response setorCreate(SetorBody setorBody) throws Exception {
		Setor setor = setorBody.criarSetor();
		daoSetor.save(setor);
		return Response.status(Status.CREATED).build();
	}
	
	 /**
     * Remove um Setor
     * 
     * @param id: id do Setor
     * @return response 200 (OK) - Conseguiu remover
	 * @throws Exception 
     */
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @DELETE
    public Response setorDelete(@PathParam("id") Integer id) throws Exception {
        daoSetor.delete(id);
    	return Response.status(Status.OK).build();
    }
    
    /**
     * Listar todos os Setores
     * 
     * @return response 200 (OK) - Conseguiu listar
	 * @throws Exception 
     */
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    @GET
    public Response setorRead() throws Exception {
        List<Setor> listSetor = daoSetor.findAll();
    	return Response.ok(listSetor).build();
    }
    
    /**
     * Atualizar Setor
     * 
     * @param id: id do Setor
     * @return response 200 (OK) - Atualizado
	 * @throws Exception 
     */
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @PUT
    public Response setorUpdate(@PathParam(value = "id") Integer id, SetorBody setorBody) throws Exception {
    	Setor setor = daoSetor.findById(id);
    	setor.setNome(setorBody.getNome());
    	Setor setorUpdated = daoSetor.update(setor);
    	return Response.ok(setorUpdated).build();
    }

}

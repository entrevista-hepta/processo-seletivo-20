package com.hepta.funcionarios.persistence;

import java.util.List;

import javax.persistence.EntityManager;

import com.hepta.funcionarios.entity.Setor;

public class SetorDAO {

	private EntityManager em;

	public SetorDAO() {
		this.em = HibernateUtil.getEntityManager();
	}

	public SetorDAO(EntityManager em) {
		this.em = em;
	}

	public void save(Setor setor) throws Exception {

		try {
			em.getTransaction().begin();
			em.persist(setor);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			e.getStackTrace();
			throw new Exception(e);
		} finally {
			em.close();
		}
	}

	public Setor findById(Integer id) throws Exception {
		Setor setor = null;
		try {
			em.getTransaction().begin();
			setor = em.find(Setor.class, id);
		} catch (Exception e) {
			e.getStackTrace();
			throw new Exception();
		}
		return setor;
	}

	public void delete(Integer id) throws Exception {
		try {
			Setor setor = em.find(Setor.class, id);
			em.getTransaction().begin();
			em.remove(setor);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			e.getStackTrace();
			throw new Exception(e);
		} finally {
			em.close();
		}

	}

	public List<Setor> findAll() throws Exception {
		List<Setor> setorList = null;
		try {
			String hql = "SELECT s FROM Setor s";
			setorList = em.createQuery(hql, Setor.class).getResultList();
		} catch (Exception e) {
			em.getTransaction().rollback();
			e.getStackTrace();
			throw new Exception(e);
		} finally {
			em.close();
		}
		return setorList;

	}

	public Setor update(Setor setor) throws Exception {
		Setor setorAtualizado = null;
		try {
			setorAtualizado = em.merge(setor);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			e.getStackTrace();
			throw new Exception(e);
		} finally {
			em.close();
		}
		return setorAtualizado;
	}

	public Setor findByName(String nome) {
		String hql = "SELECT s FROM Setor s WHERE s.nome=:nome";
		Setor singleResult = em.createQuery(hql, Setor.class).setParameter("nome", nome).getSingleResult();
		return singleResult;
	}

}

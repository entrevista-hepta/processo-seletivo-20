package com.hepta.funcionarios.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;

import javax.persistence.NoResultException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.hepta.funcionarios.entity.Setor;
import com.hepta.funcionarios.persistence.SetorDAO;
import com.hepta.funcionarios.rest.body.SetorBody;

class SetorServiceIntegrationTest {

	private SetorDAO setorDao;
	private SetorService setorService;

	@BeforeEach
	void inicializacao() throws Exception {
		setorDao = new SetorDAO();
		setorService = new SetorService();

	}

	@DisplayName("dado um SetorBody passado como par�metro, deve salvar um Setor no BD e devolve-lo")
	@Test
	void deveSalvarUmSetorNoBancoDadosEDevolverOSetorSalvo() throws Exception {
		// setup
		SetorBody setorBody = new SetorBody();
		setorBody.setNome("AlmoxarifadoTestando01");
		// execute
		setorService.setorCreate(setorBody);
		Setor resultSetor = this.setorDao.findByName(setorBody.getNome());
		// assert
		assertEquals(setorBody.getNome(), resultSetor.getNome());
	}

	@DisplayName("dado um id passado como par�metro, deve deletar um Setor no BD e devolver NoResultException pois n�o foi encontrado")
	@Test
	void deveDeletarNoBancoDadosEDevolverNull() throws Exception {
		// setup
		Setor setor = setorDao.findByName("AlmoxarifadoTestando01");//->nome do setor corresponde no banco de dados
		// execute
		setorService.setorDelete(setor.getId());
		// assert
		assertThrows(NoResultException.class,()->{
			this.setorDao.findByName(setor.getNome());
		});

	}

	@DisplayName("deve listar todos os Setores e devolver uma lista  ")
	@Test
	void deveListarTodosOsSetoresEDevolverUmaLista() throws Exception {
		// setup
		// execute
		setorService.setorRead();
		List<Setor> listaSetores = setorDao.findAll();
		// assert
		assertNotNull(listaSetores);

	}

	
	@DisplayName("dado um id e um SetorBody, esses passados como par�metro, deve atualizar Setor e devolver o nome atualizado")
	@Test
	void deveAtualizarSetorEDevolverNome() throws Exception {
		// setup
		Setor setor = setorDao.findByName("AlmoxarifadoTesteNovamente2");//->nome do setor correspondente no banco de dados
		SetorBody setorBody = new SetorBody();
		setorBody.setNome("AlmoxarifadoTesteNovamente1");
		// execute
		setorService.setorUpdate(setor.getId(), setorBody);
		Setor resultSetor = setorDao.findByName("AlmoxarifadoTesteNovamente1");
		// assert
		assertEquals(setorBody.getNome(), resultSetor.getNome());
	}

}

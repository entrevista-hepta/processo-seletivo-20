package com.hepta.funcionarios.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.hepta.funcionarios.entity.Funcionario;
import com.hepta.funcionarios.persistence.FuncionarioDAO;
import com.hepta.funcionarios.persistence.SetorDAO;
import com.hepta.funcionarios.rest.body.FuncionarioBody;

class FuncionarioServiceIntegrationTest {

	private FuncionarioService funcionarioService;
	private FuncionarioDAO funcionarioDao;
	private SetorDAO setorDao;

	@BeforeEach
	void setUpBeforeClass() {
		funcionarioDao = new FuncionarioDAO();
		setorDao = new SetorDAO();
		funcionarioService = new FuncionarioService(funcionarioDao, setorDao);

	}

	@Test
	void testFuncionarioRead() throws Exception {
		// setup
		// execute
		funcionarioService.funcionarioRead();
		List<Funcionario> listFuncionario = funcionarioDao.getAll();
		// assert
		assertNotNull(listFuncionario);

	}

	@Test
	void testFuncionarioCreate() throws Exception {
		// setup
		FuncionarioBody funcionarioBody = new FuncionarioBody();
		funcionarioBody.setNome("Joao Carnica");
		funcionarioBody.setEmail("emailpower7@gmail.com");
		funcionarioBody.setIdade(30);
		funcionarioBody.setSalario(3500.2);
		funcionarioBody.setIdSetor(43);//->id do setor existente no banco de dados
		// execute
		funcionarioService.funcionarioCreate(funcionarioBody);
		// assert
		Funcionario resultFuncionario = funcionarioDao.findByNome(funcionarioBody.getNome());
		assertEquals(funcionarioBody.getNome(),resultFuncionario.getNome());
	}

	@Test
	void testFuncionarioUpdate() throws Exception {
		// setup
		Funcionario funcionario = funcionarioDao.findByNome("Joao Barbe");//->nome do funcionario correspondente no banco de dados
		FuncionarioBody funcionarioBody = new FuncionarioBody();
		funcionarioBody.setNome("Joao Boy");
		funcionarioBody.setEmail("emailUpdated@gmail.com");
		funcionarioBody.setSalario(2300.00);
		funcionarioBody.setIdade(18);
		funcionarioBody.setIdSetor(41);//->id do setor existente no banco de dados
		// execute
		funcionarioService.funcionarioUpdate(funcionario.getId(), funcionarioBody);
		//assert
		Funcionario resultFuncionario = funcionarioDao.findByNome("Joao Boy");
		assertEquals(funcionarioBody.getEmail(), resultFuncionario.getEmail());
		
	}

	@Test
	void testFuncionarioDelete() throws Exception {
		// setup
		Funcionario funcionario = funcionarioDao.findByNome("Joao Barbe");//->nome do funcionario correspondente no banco de dados
		// execute
		funcionarioService.funcionarioDelete(funcionario.getId());
		Funcionario resultFuncionario = funcionarioDao.find(funcionario.getId());
		// assert
		assertEquals(null, resultFuncionario);

	}

}

package com.hepta.funcionarios.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.hepta.funcionarios.entity.Funcionario;
import com.hepta.funcionarios.entity.Setor;
import com.hepta.funcionarios.persistence.FuncionarioDAO;
import com.hepta.funcionarios.persistence.SetorDAO;
import com.hepta.funcionarios.rest.body.FuncionarioBody;

class FuncionarioServiceTest {

	private FuncionarioService funcionarioService;
	@Mock
	private FuncionarioDAO daoFuncionario;
	@Mock
	private SetorDAO daoSetor;

	@BeforeEach
	void setUpBeforeClass() {
		MockitoAnnotations.initMocks(this);
		funcionarioService = new FuncionarioService(daoFuncionario, daoSetor);

	}

	@Test
	void testFuncionarioRead() throws Exception {
		// setup
		List<Funcionario> listaDeFuncionario = listaDeFuncionario();
		Mockito.when(daoFuncionario.getAll()).thenReturn(listaDeFuncionario);
		// execute
		Response reponseResult = funcionarioService.funcionarioRead();
		// verify and assert
		verify(daoFuncionario).getAll();
		int status = Response.status(Status.OK).entity(listaDeFuncionario).build().getStatus();
		assertEquals(status, reponseResult.getStatus());

	}

	@Test
	void testFuncionarioReadException() throws Exception {
		// setup
		Mockito.when(daoFuncionario.getAll()).thenThrow(Exception.class);
		// execute
		Response reponseResult = funcionarioService.funcionarioRead();
		// verify and assert
		verify(daoFuncionario).getAll();
		int status = Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao buscar Funcionarios").build()
				.getStatus();
		assertEquals(status, reponseResult.getStatus());

	}

	@Test
	void testFuncionarioCreate() throws Exception {
		// setup
		Integer id = Integer.parseInt("1");
		Setor setor = new Setor();
		setor.setId(id);
		
		FuncionarioBody funcionarioBody = new FuncionarioBody();

		Mockito.when(daoSetor.findById(funcionarioBody.getIdSetor())).thenReturn(setor);
		Mockito.doNothing().when(daoFuncionario).save(ArgumentMatchers.any(Funcionario.class));
		// execute
		Response responseResult = funcionarioService.funcionarioCreate(funcionarioBody);
		// verify and assert
		verify(daoSetor).findById(funcionarioBody.getIdSetor());
		verify(daoFuncionario).save(ArgumentMatchers.any(Funcionario.class));
		int status = Response.status(Status.CREATED).build().getStatus();
		assertEquals(status, responseResult.getStatus());
	}

	@Test
	void testFuncionarioUpdate() throws Exception {
		// setup
		Integer id = Integer.parseInt("1");
		FuncionarioBody funcionarioBody = new FuncionarioBody();
		Setor setor = new Setor();
		Funcionario funcionario = new Funcionario();
		funcionario.setSetor(setor);

		Mockito.when(daoFuncionario.find(id)).thenReturn(funcionario);
		Mockito.when(daoFuncionario.update(funcionario)).thenReturn(funcionario);
		// execute
		Response responseResult = funcionarioService.funcionarioUpdate(id, funcionarioBody);
		// verify and assert
		verify(daoFuncionario).find(id);
		verify(daoFuncionario).update(funcionario);
		int status = Response.status(Status.OK).entity(funcionarioBody).build().getStatus();
		assertEquals(status, responseResult.getStatus());

	}

	@Test
	void testFuncionarioDelete() throws Exception {
		// setup
		Integer id = Integer.parseInt("1");
		Mockito.doNothing().when(daoFuncionario).delete(id);
		// execute
		Response reponseResult = funcionarioService.funcionarioDelete(id);
		// verify and assert
		verify(daoFuncionario).delete(id);
		int status = Response.status(Status.OK).build().getStatus();
		assertEquals(status, reponseResult.getStatus());

	}

	private List<Funcionario> listaDeFuncionario() {
		Funcionario funcionario = new Funcionario();
		List<Funcionario> lista = new ArrayList<>();
		lista.add(funcionario);
		return lista;

	}

}

package com.hepta.funcionarios.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.hepta.funcionarios.entity.Setor;
import com.hepta.funcionarios.persistence.SetorDAO;
import com.hepta.funcionarios.rest.body.SetorBody;

class SetorServiceTest {
	
	private SetorService setorService;
	@Mock
	private SetorDAO daoSetor;
	
	@BeforeEach
	void inicializacao() {
		MockitoAnnotations.initMocks(this);
		setorService = new SetorService(this.daoSetor);
	}

	@DisplayName("dado um SetorBody passado como par�metro, deve criar um Setor e devolver status CREATED  ")
	@Test
	void deveSalvarUmSetorNoBancoDadosEDevolverUmStatusCreated() throws Exception {
		//setup
		SetorBody setorBody =  new SetorBody();
		setorBody.setNome("Almoxarifado");
		Mockito.doNothing().when(daoSetor).save(ArgumentMatchers.any(Setor.class));
		//execute
		Response responseResult = setorService.setorCreate(setorBody);
		//verify and assert
		verify(daoSetor).save(ArgumentMatchers.any(Setor.class));
		int statusCode = Response.Status.CREATED.getStatusCode();
		assertEquals(statusCode, responseResult.getStatus());
		 

	}
	
	@DisplayName("dado um id passado como par�metro, deve deletar um Setor e devolver status OK  ")
	@Test
	void deveDeletarNoBancoDadosEDevolverUmStatusOK() throws Exception {
		//setup
		Integer id = Integer.parseInt("1");
		Mockito.doNothing().when(daoSetor).delete(id);
		//execute
		Response setorResult = setorService.setorDelete(id);
		//verify and assert
		verify(daoSetor).delete(id);
		int statusCode = Response.Status.OK.getStatusCode();
		assertEquals(statusCode , setorResult.getStatus());

	}
	
	@DisplayName("deve deve listar todos os Setores e devolver status OK  ")
	@Test
	void deveListarTodosOsSetoresEDevolverUmStatusOK() throws Exception {
		//setup
		List<Setor> listaDeSetores = listaDeSetores();
		Mockito.when(daoSetor.findAll()).thenReturn(listaDeSetores);
		//execute
		Response setorResult = setorService.setorRead();
		//verify and assert
		verify(daoSetor).findAll();
		int statusCode = Response.Status.OK.getStatusCode();
		assertEquals(statusCode, setorResult.getStatus());
//		Object entity = setorResult.getEntity();
	
	}
	
	@DisplayName("dado um id e um setorBody, ambos passados como par�metro, deve atualizar Setor e devolver status OK")
	@Test
	void deveAtualizarSetorEDevolverUmStatusOK() throws Exception {
		//setup
		Integer id = Integer.parseInt("1");
		Setor setorBD = new Setor();
		setorBD.setId(1);
		
		SetorBody setorBody =  new SetorBody();
		setorBody.setNome("Almoxarifado");
		
		setorBD.setNome(setorBody.getNome());
		
		Mockito.when(daoSetor.findById(id)).thenReturn(setorBD);
		Mockito.when(daoSetor.update(setorBD)).thenReturn(setorBD);
		//execute
		Response setorResult = setorService.setorUpdate(id, setorBody);
		//verify and assert
		verify(daoSetor).findById(id);
		verify(daoSetor).update(setorBD);
		int statusCode = Response.Status.OK.getStatusCode();
		assertEquals(statusCode, setorResult.getStatus());
	}
	
	private List<Setor> listaDeSetores(){
		SetorBody setorBody = new SetorBody();
		setorBody.setNome("TesteSetor");
		Setor setor = setorBody.criarSetor();
		
		List<Setor> lista = new ArrayList<>();
		lista.add(setor);
		return lista;
		
	}

}
